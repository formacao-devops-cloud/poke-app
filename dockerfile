FROM node:14.20.1-alpine as builder

WORKDIR /home/node/app

COPY package*.json ./

RUN npm i

COPY . ./

RUN npm run build

FROM nginx:1.15 as server

COPY --from=builder /home/node/app/build /usr/share/nginx/html